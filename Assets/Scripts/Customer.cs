﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Customer : MonoBehaviour {

    public Item ItemToBuy { get; private set; }
    public float CurrentOffer { get; private set; }

    public void ChooseItem(List<Item> items)
    {
        if (items.Count <= 0) {
            throw new NoItemsInShopException(
            "Customer can not choose item because " +
            "there are none in the player inventory");
        }
        ItemToBuy = items[UnityEngine.Random.Range(0, items.Count)];
    }

    public float MakeOffer()
    {
        CurrentOffer = UnityEngine.Random.Range(1f, 2f) * ItemToBuy.BasePrice;
        CurrentOffer = Money.Format(CurrentOffer);
        return CurrentOffer;
    }

    public class NoItemsInShopException : Exception
    {
        public NoItemsInShopException(string message) : base(message) { }
    }
}
