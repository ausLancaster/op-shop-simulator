﻿using UnityEngine;

public class BusinessDayDetails {

    public BusinessDayDetails()
    {
        Reset();
    }

    public int ItemsSold { get; set; }
    public float Expenses { get; set; }
    public float Income { get; set; }
    public float Profit {
        get
        {
            return Income - Expenses;
        }
    }

    public void Reset()
    {
        ItemsSold = 0;
        Expenses = 0;
        Income = 0;
    }
}
