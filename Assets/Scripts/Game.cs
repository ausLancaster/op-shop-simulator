﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Game : MonoBehaviour {

    [SerializeField]
    ShopUIController shopUI;
    [SerializeField]
    BarteringUIController barteringUI;
    [SerializeField]
    CustomerFactory customerFactory;
    [SerializeField]
    PlayerInventory playerInventory;
    [SerializeField]
    ShopInventory shopInventory;
    [SerializeField]
    float waitMin, waitMax;
    
    const float DAY_LENGTH = 8f;
    float time = 0;

    Customer CurrentCustomer {
        get
        {
            return barteringUI.CurrentCustomer;
        }
        set
        {
            barteringUI.CurrentCustomer = value;
        }
    }

    void Start()
    {
        GoToShop();

    }

    public void GoToBartering()
    {
        shopUI.gameObject.SetActive(false);
        barteringUI.gameObject.SetActive(true);
        time = 0;
        //barteringUI.SetClock(0);
        if (CurrentCustomer == null) StartWaiting();
    }

    public void GoToShop()
    {
        playerInventory.DayDetails.Reset();
        shopInventory.Restock();
        barteringUI.gameObject.SetActive(false);
        shopUI.gameObject.SetActive(true);

    }

    public void NewCustomer()
    {
        CurrentCustomer = customerFactory.Get();
        List<Item> items = playerInventory.Items;
        CurrentCustomer.ChooseItem(items);
        CurrentCustomer.MakeOffer();
        barteringUI.ShowNewCustomer(CurrentCustomer);
    }

    public void Sell()
    {
        playerInventory.SellItem(CurrentCustomer.ItemToBuy,
                                CurrentCustomer.CurrentOffer);
        playerInventory.DayDetails.ItemsSold += 1;
        playerInventory.DayDetails.Income += CurrentCustomer.CurrentOffer;
        Destroy(CurrentCustomer.ItemToBuy.gameObject);
        StartWaiting();
    }

    public void StartWaiting()
    {
        if (CurrentCustomer != null)
        {
            barteringUI.CustomerEnabled(false);
            Destroy(CurrentCustomer.gameObject);
        }
        barteringUI.StartWaiting();
        if (!playerInventory.IsEmpty()) {
            StartCoroutine(WaitForCustomer());
        }
        else
        {
            StartCoroutine(barteringUI.InventoryEmpty());
        }

    }

    IEnumerator WaitForCustomer() {

        float waitLength = Random.Range(0.5f, 3.0f);
        float clockFrameMax = 0.2f;
        float clockFrame = 0;
        for (float t = 0; t < waitLength; t += Time.unscaledDeltaTime)
        {
            while (clockFrame > clockFrameMax)
            {
                barteringUI.SetClock(time / DAY_LENGTH);
                clockFrame -= clockFrameMax;
            }

            if (time > DAY_LENGTH)
            {
                barteringUI.EndOfDay();
                yield break;
            }

            clockFrame += Time.unscaledDeltaTime;

            time += Time.unscaledDeltaTime;
            yield return null;
        }
        NewCustomer();
    }
}