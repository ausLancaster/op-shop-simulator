﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarteringUIController : MonoBehaviour {

    [SerializeField]
    Camera camera;
    [SerializeField]
    Color background;
    [SerializeField]
    ItemDetails itemDetails;
    [SerializeField]
    GameObject saleDetails;
    [SerializeField]
    Text offerText;
    [SerializeField]
    Text inventoryEmptyText;
    [SerializeField]
    GameObject endOfDay;
    [SerializeField]
    Text endOfDayDetails;
    [SerializeField]
    Image clock;
    [SerializeField]
    PlayerInventory playerInventory;
    [SerializeField]
    GameObject doneButton;

    public Customer CurrentCustomer { get; set; }

    private void OnEnable()
    {
        camera.backgroundColor = background;
        inventoryEmptyText.enabled = false;
        doneButton.SetActive(false);
        saleDetails.SetActive(true);
        itemDetails.gameObject.SetActive(true);
        if (CurrentCustomer != null) CustomerEnabled(true);
    }

    private void OnDisable()
    {
        if (CurrentCustomer != null) CustomerEnabled(false);
        endOfDay.SetActive(false);
    }

    public void ShowNewCustomer(Customer customer)
    {
        if (CurrentCustomer) CustomerEnabled(false);
        CurrentCustomer = customer;
        CustomerEnabled(true);
        CurrentCustomer.ItemToBuy.transform.localPosition = new Vector3(-277f, 44.5f, 0f);
        CurrentCustomer.ItemToBuy.transform.localScale = new Vector3(0.75f, 0.75f, 0.75f);
        itemDetails.UpdateDetails(CurrentCustomer.ItemToBuy, "You Paid");
        offerText.text = '$' + CurrentCustomer.MakeOffer().ToString();
    }

    public IEnumerator InventoryEmpty()
    {
        saleDetails.SetActive(false);
        itemDetails.gameObject.SetActive(false);
        inventoryEmptyText.enabled = true;
        if (CurrentCustomer) CustomerEnabled(false);
        yield return new WaitForSeconds(2.0f);
        EndOfDay();

    }

    public void StartWaiting() {
        saleDetails.SetActive(false);
        itemDetails.gameObject.SetActive(false);
    }

    public void CustomerEnabled(bool boolean)
    {
        CurrentCustomer.GetComponent<Image>().enabled = boolean;
        CurrentCustomer.ItemToBuy.GetComponent<Image>().enabled = boolean;
        saleDetails.SetActive(true);
        itemDetails.gameObject.SetActive(true);
    }

    public void SetClock(float fraction) {
        clock.material.SetFloat("_TimeFraction", fraction);
    }

    public void EndOfDay()
    {
        clock.material.SetFloat("_TimeFraction", 1f);
        inventoryEmptyText.enabled = false;
        saleDetails.SetActive(false);
        itemDetails.gameObject.SetActive(false);
        endOfDay.SetActive(true);
        endOfDayDetails.text =
            "Items Sold: " + playerInventory.DayDetails.ItemsSold + "\n" +
            "Expenses: " + playerInventory.DayDetails.Expenses + "\n" +
            "Income: " + playerInventory.DayDetails.Income + "\n" +
            "Profit: " + playerInventory.DayDetails.Profit + "\n";
        doneButton.SetActive(true);
    }

}