﻿using UnityEngine;

public class ShopInventory : MonoBehaviour {

    [SerializeField]
    ItemFactory factory;
    Item[] items;
    int current = 0;

    void Awake () 
    {
        Initialise(ItemType.JUMPER, 10);
	}

    void Initialise (ItemType type, int n)
    {
        items = new Item[n];
        for (int i=0; i<items.Length; i++)
        {
            items[i] = factory.Get(type);
        }
    }

    public Item GetCurrent()
    {
        return items[current];
    }

    public bool HasNext () 
    {
        return current < items.Length - 1;
    }

    public bool HasPrevious ()
    {
        return current > 0;
    }

    public Item Next ()
    {
        if (!HasNext())
        {
            throw new System.IndexOutOfRangeException();
        }
        current++;
        return GetCurrent();
    }

    public Item Previous ()
    {
        if (!HasPrevious())
        {
            throw new System.IndexOutOfRangeException();
        }
        current--;
        return GetCurrent();
    }

    public void Restock() { 
        for (int i=0; i<items.Length; i++) {
            if (items[i] == null || items[i].Sold)
            {
                items[i] = factory.Get(ItemType.JUMPER);
            }
        }
    }

    public void ResetCurrent() {
        current = 0;
    }
}
