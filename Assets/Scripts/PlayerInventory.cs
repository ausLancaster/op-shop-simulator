﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInventory : MonoBehaviour {

    const float INITIAL_MONEY = 30f;

    [SerializeField]
    UnityEngine.UI.Text moneyText;
    public List<Item> Items { get; private set; }
    float maxItems = 5;
    float money;
    public BusinessDayDetails DayDetails { get; set; }

    void Start()
    {
        DayDetails = new BusinessDayDetails();
    }

    public float Money
    {
        get
        {
            return money;
        }
        set
        {
            money = value;
            moneyText.text = "$" + money.ToString();
        }
    }

    int Count() {
        return Items.Count;
        }
        
    public bool IsEmpty()
    {
        return Items.Count == 0;
    }

    private void Awake()
    {
        Money = INITIAL_MONEY;
        Items = new List<Item>();
    }

    public void BuyItem(Item item)
    {
        if (Items.Count >= maxItems)
        {
            throw new NoSpaceException("Not enough inventory space\n");
        }
        else if (item.BasePrice > Money) {
            throw new NotEnoughMoneyException("Player does not have enough money\n");
        }
        else
        {
            Money -= item.BasePrice;
            Items.Add(item);
        }
    }

    public void SellItem(Item item, float price)
    {
        Money += price;
        Items.Remove(item);
    }

    public class NoSpaceException : Exception
    {
        public NoSpaceException(string message) : base(message) { }
    }

    public class NotEnoughMoneyException : Exception
    {
        public NotEnoughMoneyException(string message) : base(message) { }
    }
}
