﻿using UnityEngine;
using System;

public class ItemFactory : MonoBehaviour {

    [SerializeField]
    Transform parent;
    [SerializeField]
    Item[] prefabs;

    public Item Get(ItemType type)
    {
        int seed = UnityEngine.Random.Range(0, 10000);// ^ (int)Time.unscaledTime;
        Item item = Instantiate(prefabs[(int)type.Index], parent);

        item.Initialise(type, GeneratePrice(type), seed);

        return item;
    }

    public static float GeneratePrice(ItemType type)
    {
        float r = UnityEngine.Random.value;
        if (type == ItemType.JUMPER)
        {
            float result = PyramidDistribution(10f, 0.5f, r);
            return Money.Format(result);
        }
        throw new ArgumentException("Item type not supported");
    }

    public static float PyramidDistribution(
        float average,
        float lower_ratio,
        float r)
    {
        float new_r = r;
        if (r > 0.5f) new_r = 1f - r;

        float min = lower_ratio * average;
        float width = average - min;
        float tan = 1f / (width * width);
        float x = Mathf.Sqrt((2f * new_r) / tan);

        return r < 0.5f ? (1 / 2f) * average + x : (3 / 2f) * average - x;

    }

}
