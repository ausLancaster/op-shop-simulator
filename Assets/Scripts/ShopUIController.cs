﻿using UnityEngine;
using UnityEngine.UI;

public class ShopUIController : MonoBehaviour {

    [SerializeField]
    ShopInventory shopInventory;
    [SerializeField]
    PlayerInventory playerInventory;
    [SerializeField]
    Text leftArrow;
    [SerializeField]
    Text rightArrow;
    [SerializeField]
    ItemDetails itemDetails;
    [SerializeField]
    GameObject buyButton;
    [SerializeField]
    Text noMoneyText;
    [SerializeField]
    GameObject soldSign;
    [SerializeField]
    Camera camera;
    [SerializeField]
    Color background;
        
    private void OnEnable()
    {
        camera.backgroundColor = background;
        shopInventory.ResetCurrent();
        CurrentItemImageEnabled(true);
        UpdateOnClick();
    }

    private void OnDisable()
    {
        CurrentItemImageEnabled(false);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow) ||
        Input.GetKeyDown(KeyCode.D))
        {
            Next();
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow) ||
        Input.GetKeyDown(KeyCode.A))
        {
            Previous();
        }
    }

    public void Next()
    {
        if (shopInventory.HasNext())
        {
            CurrentItemImageEnabled(false);
            shopInventory.Next();
            CurrentItemImageEnabled(true);
            UpdateOnClick();
        }
    }

    public void Previous()
    {
        if (shopInventory.HasPrevious())
        {
            CurrentItemImageEnabled(false);
            shopInventory.Previous();
            CurrentItemImageEnabled(true);
            UpdateOnClick();
        }
    }

    public void CurrentItemImageEnabled(bool enabled)
    {
        shopInventory.GetCurrent().GetComponent<Image>().enabled = enabled;
    }

    void UpdateOnClick()
    {
        Item current = shopInventory.GetCurrent();
        itemDetails.UpdateDetails(current, "Price");

        if (current.Sold)
        {
            soldSign.SetActive(true);
            buyButton.SetActive(false);
            noMoneyText.enabled = false;
        }
        else
        {
            soldSign.SetActive(false);

            if (playerInventory.Money < current.BasePrice)
            {
                buyButton.SetActive(false);
                noMoneyText.enabled = true;
            }
            else
            {
                buyButton.SetActive(true);
                noMoneyText.enabled = false;
            }
        }

        if (shopInventory.HasNext())
        {
            rightArrow.enabled = true;
        }
        else
        {
            rightArrow.enabled = false;
        }

        if (shopInventory.HasPrevious())
        {
            leftArrow.enabled = true;
        }
        else
        {
            leftArrow.enabled = false;
        }
    }

    public void Buy()
    {
        Item current = shopInventory.GetCurrent();
        playerInventory.BuyItem(current);
        playerInventory.DayDetails.Expenses += current.BasePrice;
        current.Sold = true;
        UpdateOnClick();
    }
}
