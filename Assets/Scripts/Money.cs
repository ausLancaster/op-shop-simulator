﻿using System;

public static class Money {

    private const float INCREMENT = 0.5f;

    public static float Format(float val)
    {
        return (float)Math.Round(val / INCREMENT) * INCREMENT;
    }
}
