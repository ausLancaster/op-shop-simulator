﻿using System.Collections.Generic;

public class ItemType {

    public static readonly ItemType JUMPER = new ItemType(0, 10.0f);
    public static readonly ItemType SHIRT = new ItemType(1, 6.0f);
    public static readonly ItemType SKIRT = new ItemType(2, 8.0f);
    public static readonly ItemType HAT = new ItemType(3, 6.0f);
    public static readonly ItemType JACKET = new ItemType(4, 16.0f);

    public static IEnumerator <ItemType> Values
    {
        get
        {
            yield return JUMPER;
            yield return SHIRT;
            yield return SKIRT;
            yield return HAT;
            yield return JACKET;
        }
    }

    public int Index { get; private set; }
    public float BasePrice { get; private set; }

    ItemType(int index, float basePrice)
    {
        Index = index;
        BasePrice = basePrice;
    }
}