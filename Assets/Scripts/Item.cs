﻿using UnityEngine;

public class Item : MonoBehaviour {

    public ItemType Type { get; private set; }
    public float BasePrice { get; private set; }
    public int Seed { get; private set; }
    public bool Sold { get; set; }




    public void Initialise(ItemType type, float basePrice, int seed)
    {
        this.Type = type;
        this.BasePrice = basePrice;
        this.Seed = seed;
        UnityEngine.UI.Image image = GetComponent<UnityEngine.UI.Image>();
        image.material = new Material(image.material);
        image.material.SetInt("_Seed", seed);
    }
}