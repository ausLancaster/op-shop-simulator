﻿using UnityEngine;

public class ItemDetails : MonoBehaviour {

    [SerializeField]
    UnityEngine.UI.Text typeText;
    [SerializeField]
    UnityEngine.UI.Text priceText;

    public void UpdateDetails(Item item, string priceDescription)
    {
        typeText.text = item.Type.ToString() + " #" + item.Seed.ToString();
        priceText.text = priceDescription + ": $" + item.BasePrice.ToString();
    }
}
