﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CustomerFactory : MonoBehaviour {

    [SerializeField]
    Transform parent;
    [SerializeField]
    Customer prefab;
    [SerializeField]
    Sprite[] sprites;

    public Customer Get()
    {
        Customer customer = Instantiate(prefab, parent);
        customer.GetComponent<Image>().sprite = 
            sprites[Random.Range(0, sprites.Length)];
        return customer;
    }
}
