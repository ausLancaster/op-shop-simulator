﻿Shader "Custom/Clock"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
        _Color1("Clock Color 1", Color) = (1, 1, 1, 1)
        _Color2("Clock Color 2", Color) = (0, 0, 0, 1)
        _BackgroundColor("Background Color", Color) = (0, 0, 0, 0)
        _TimeFraction("Time", Float) = 0.25
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
       
float4 _Color1, _Color2, _BackgroundColor;
float _TimeFraction;

#define PI 3.14159265359

fixed4 frag (v2f i) : SV_Target
            {
                i.uv -= float2(0.5, 0.5);
                float a = (-atan2(i.uv.y, i.uv.x) + PI * 0.5) / (PI * 2.0);
                a = frac(a);
                float r = 0.5 - length(i.uv);
                clip(r);
                if (a < _TimeFraction) {
                    return _Color1;                
                } else {
                    return _Color2;
                }

                            
            }

			ENDCG
		}
	}
}
